export default {
  target: 'server',

  env: {
    appUrl: process.env.APP_URL || 'https://localhost:3000'
  },

  head: {
    titleTemplate: t => t ? `MorganLee · ${t}` : 'MorganLee · Freelance Developer',

    htmlAttrs: {
      lang: 'en'
    },

    meta: [
      { charset: 'UTF-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0'
      },
      {
        name: 'description',
        content: 'Experienced freelance developer offering responsive and modern websites with an inexpensive price tag.'
      },
      {
        name: 'og:description',
        content: 'Experienced freelance developer offering responsive and modern websites with an inexpensive price tag.'
      },


      { hid: 'og:title', name: 'og:title', content: 'MorganLee · Freelancer, Developer' },
      { name: 'theme-color', content: '#2563eb' },
      { name: 'keywords', content: 'morgan, nodejs, discord, discordjs, discord bots, websites, web, development, service, web developer, freelance' },
      { name: 'og:type', content: 'article' },
      { hid: 'og:url', name: 'og:url', content: process.env.APP_URL },
      { name: 'url', content: process.env.APP_URL },
      { name: 'robots', content: 'index,follow' },

      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { name: 'apple-touch-fullscreen', content: 'yes' },
      { name: 'apple-mobile-web-app-status-bar-style', content: 'black' },
      { name: 'format-detection', content: 'telephone=no' },

      { hid: 'og:image', name: 'og:image', content: `${process.env.APP_URL}/logo.jpg`},
      { hid: 'twitter:image', name: 'twitter:image', content: `${process.env.APP_URL}/logo.jpg`},
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary'},

      { rel: 'icon', type: 'image/png', sizes: '16x1', href: `${process.env.APP_URL}/logo.jpg` },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: `${process.env.APP_URL}/logo.jpg` },
      { rel: 'icon', type: 'image/png', sizes: '96x96', href: `${process.env.APP_URL}/logo.jpg` },
      { rel: 'shortcut icon', type: 'image/x-icon', href: `${process.env.APP_URL}/logo.jpg` },
    ],

    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/logo.png' },
      { rel: 'stylesheet', href: 'https://rsms.me/inter/inter.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css' }
    ],

    script: [
      {
        "data-website-id": "43686c01-7912-4dfb-89ac-d1bebaf195b7", 
        "src": "https://stats.tbdscripts.com/umami.js",
        async: true,
        defer: true
      }
    ]
  },

  css: [],

  plugins: [],

  components: true,

  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/dotenv'
  ],

  serverMiddleware: [
    { path: '/api/cdn', handler: '~/api/cdn/index.ts' },
    { path: '/api/spotify', handler: '~/api/spotify/index.ts' },
    { path: '/api/discord', handler: '~/api/discord.ts' }
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy'
  ],

  axios: {
    proxy: true
  },

  proxy: {
    '/api/': { target: 'https://morgan-lee.cc/api', pathRewrite: {'^/api/': ''}, changeOrigin: true }
  },

};
