interface PaginatedResults<T = any> {
  results: T[];
  meta: {
    results: number;
    perPage: number;
    page: number;
    maxPage: number;
  }
}

interface IListFile {
  id: string;
  size: number;
  src: string;
  createdAt: number;
}


interface IFile extends IListFile {
  name: string;
}
