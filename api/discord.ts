import express, { Request, Response } from 'express'
import Axios from 'axios'
import cors from 'cors';

const app = express();
app.use(cors());

interface DiscordResData {
  id: string;
  username: string;
  discriminator: string;
  avatar: string;
  public_flags: string;
}

app.get('/', async (req: Request, res: Response<DiscordResData | null>) => {
  const response = await Axios.get<DiscordResData>('https://discord.com/api/v8/users/294197961112682498', {
    withCredentials: true,
    headers: {
      ['Authorization']: `Bot ${process.env.DISCORD_BOT_TOKEN}`,
      'Access-Control-Allow-Origin': '*',
    }
  }).catch((err) => {
    console.error(err);
  });

  if (!response || response.status !== 200 ) {
    return res.status(204).send();
  } else {
    return res.json({
      id: response.data.id,
      avatar: response.data.avatar,
      username: response.data.username,
      public_flags: response.data.public_flags,
      discriminator: response.data.discriminator
    });
  }
})
module.exports = app;
