import express, {Request, Response} from 'express';
import { makeSpotifyRequest } from "./_requests";
import cors from 'cors'

const app = express();
app.use(cors());

interface SpotifyData {
  is_playing: boolean;
  item: {
    name: string;

    artists: Array<{
      name: string;
    }>;

    album: {
      images: Array<{
        url: string;
      }>;
    };

    external_urls: {
      spotify: string;
    };
  };
}

interface SpotifyResponseData {
  song: string;
  artist: string;
  image: string;
  url: string;
}

app.get('/', async (req: Request, res: Response<SpotifyResponseData | null>) => {
  const response = await makeSpotifyRequest<SpotifyData>('me/player')
    .catch(e => console.error(`spotify error`, e));

  if (!response || response.status !== 200 || !response.data.is_playing) {
    return res.status(204).send();
  } else {
    return res.json({
      song: response.data.item.name,
      artist: response.data.item.artists[0]?.name,
      image: response.data.item.album.images[0]?.url,
      url: response.data.item.external_urls.spotify
    });
  }
});

module.exports = app;
