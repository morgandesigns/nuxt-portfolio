import Axios, { AxiosError } from "axios";
import { readFileSync, writeFileSync } from "fs";
import { resolve } from 'path';
const { stringify } = require('qs')

let _token = process.env.SPOTIFY_ACCESS_TOKEN;

async function rotateToken() {
  try {
    const res = await Axios.post('https://accounts.spotify.com/api/token', stringify({
      grant_type: 'refresh_token',
      refresh_token: process.env.SPOTIFY_REFRESH_TOKEN,
      client_id: process.env.SPOTIFY_CLIENT_ID,
      client_secret: process.env.SPOTIFY_CLIENT_SECRET
    }), {
      headers: {
        ['Content-Type']: 'application/x-www-form-urlencoded',
        ['Accept']: 'application/json'
      }
    });

    _token = res.data.access_token;

    let envContents = readFileSync(resolve(process.cwd(), '.env'), 'utf-8');
    envContents = envContents.replace(/SPOTIFY_ACCESS_TOKEN=(.*)/gm, `SPOTIFY_ACCESS_TOKEN=${res.data.access_token}`);

    writeFileSync(resolve(process.cwd(), '.env'), envContents);
    console.log(`Spotify: Token rotated to ${res.data.access_token}`);
  } catch(e) {
    console.error('Spotify: Failed to rotate token', e, e.response.data);
  }
}

export async function makeSpotifyRequest<T = any>(path: string, shouldFail?: boolean) {
  try {
    return await Axios.get<T>(`https://api.spotify.com/v1/${path}`, {
      headers: {
        ['Authorization']: `Bearer ${_token}`,
        ['Access-Control-Allow-Origin']: '*',
      }
    });
  } catch(e: any) {
    console.log(e.response.status === 401)

    if (!shouldFail && [401, 403].includes(e.response?.status)) {
      await rotateToken();

      return await makeSpotifyRequest<T>(path, true);
    } else {
      throw e;
    }
  }
}
