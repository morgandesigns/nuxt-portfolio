export function paginateResults<T = any>(perPage: number, page: number, rawResults: T[]): PaginatedResults {
  page = Math.max(page, 1);

  const results = rawResults.slice((page - 1) * perPage, page * perPage);

  return {
    results,
    meta: {
      page,
      perPage,
      maxPage: Math.ceil(rawResults.length / perPage),
      results: rawResults.length
    }
  }
}
