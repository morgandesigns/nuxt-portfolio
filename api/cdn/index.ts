import express, { Request, Response, NextFunction } from 'express';
import { assertBucket, deleteFile, emptyBucket, getFile, listFiles, uploadFile } from "./_minio";
import { paginateResults } from "./_helpers";
import fileUpload, { UploadedFile } from "express-fileupload";
import cors from 'cors';

assertBucket();

const app = express();
app.use(cors());

app.use(fileUpload({
  limits: {
    fileSize: 25 * 1024 * 1024
  },
  abortOnLimit: true
}));

function authenticate(req: Request, res: Response, next: NextFunction) {
  if (req.header('Authorization') !== process.env.CDN_AUTH_TOKEN) return res.status(403).send();

  return next();
}

function clamp(val: number, min: number, max: number) {
  return Math.min(Math.max(min, val), max);
}

// List endpoint
app.get('/', async (req: Request, res: Response<PaginatedResults<IListFile>>) => {
  const page = parseInt(<string>req.query.page ?? '1');
  const perPage = parseInt(<string>req.query.perPage ?? '16');
  const sortBy = <string>req.query.sortBy ?? 'newest-first';

  try {
    let files = await listFiles();

    switch(sortBy) {
      default:
      case 'newest-first': {
        files = files.sort((a, b) => b.createdAt - a.createdAt);
        break;
      }

      case 'oldest-first': {
        files = files.sort((a, b) => a.createdAt - b.createdAt);
        break;
      }

      case 'size-asc': {
        files = files.sort((a, b) => a.size - b.size);
        break;
      }

      case 'size-desc': {
        files = files.sort((a, b) => b.size - a.size);
        break;
      }

    }

    return res.json(paginateResults<IListFile>(clamp(perPage, 4, 20), page, files));
  } catch(e) {
    console.error(e);

    res.status(500).send();
  }
})

// Retrieve specific image data
app.get('/:id', async (req: Request, res: Response<IFile | undefined>) => {
  const file = await getFile(req.params.id);
  if (!file) return res.status(404).send();

  return res.json(file);
});

// Upload a new image
app.post('/', authenticate, async (req: Request, res: Response<IFile | undefined>) => {
  const file = req.files?.file as UploadedFile;
  if (!file) return res.status(400).send();

  return res.json(await uploadFile(file));
});

// Delete **all** images
app.delete('/', authenticate, async (req: Request, res: Response) => {
  await emptyBucket();

  return res.status(200).send();
});


// Delete specific image
app.delete('/:id', authenticate, async (req: Request, res: Response) => {
  await deleteFile(req.params.id);

  return res.status(200).send();
});

module.exports = app;
