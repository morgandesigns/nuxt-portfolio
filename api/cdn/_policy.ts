export function createPolicy(bucket: string) {
  return {
    "Statement": [
      {
        "Action": [
          "s3:GetBucketLocation"
        ],
        "Effect": "Allow",
        "Principal": {
          "AWS": [
            "*"
          ]
        },
        "Resource": [
          `arn:aws:s3:::${bucket}`
        ]
      },
      {
        "Action": [
          "s3:ListBucket"
        ],
        "Effect": "Deny",
        "Principal": {
          "AWS": [
            "*"
          ]
        },
        "Resource": [
          `arn:aws:s3:::${bucket}`
        ]
      },
      {
        "Action": [
          "s3:GetObject"
        ],
        "Effect": "Allow",
        "Principal": {
          "AWS": [
            "*"
          ]
        },
        "Resource": [
          `arn:aws:s3:::${bucket}/*`
        ]
      }
    ],
    "Version": "2012-10-17"
  }
}
