import * as minio from 'minio';
import { UploadedFile } from "express-fileupload";

import { createPolicy } from "./_policy";
import { BucketItem } from "minio";

function getConfig() {
  const endpoint = process.env.MINIO_ENDPOINT as string;
  const endpointURL = new URL(endpoint);

  return {
    publicUrl: process.env.MINIO_PUBLIC_URL as string,
    bucket:  process.env.MINIO_BUCKET as string || 'storage',
    endPoint: endpointURL.hostname as string,
    useSSL: endpointURL.protocol === "https",
    accessKey: process.env.MINIO_ACCESS_KEY as string,
    secretKey: process.env.MINIO_SECRET_KEY as string,
    ...(endpointURL.port ? { port: parseInt(endpointURL.port) } : {})
  };
}

const config = getConfig();
export const client = new minio.Client(config);

export async function assertBucket() {
  const exists = await client.bucketExists(config.bucket);
  if (exists) return;

  await client.makeBucket(config.bucket, "eu-central-1");

  await client.setBucketPolicy(config.bucket, JSON.stringify(createPolicy(config.bucket)));

  console.log(`Minio: Asserted bucket (${config.bucket})`);
}

export function listFiles(): Promise<IListFile[]> {
  return new Promise<IListFile[]>((resolve, reject) => {
    const items: BucketItem[] = [];

    const stream = client.listObjects(config.bucket);

    stream.on('data', file => items.push(file));
    stream.on('error', reject);

    stream.on('end', () => {
      resolve(items.map(i => ({
        id: i.name,
        size: i.size,
        src: `${config.publicUrl}/${config.bucket}/${i.name}`,
        createdAt: i.lastModified.getTime(),
      })))
    });
  });
}

export async function getFile(objectId: string): Promise<IFile | undefined> {
  try {
    const file = await client.statObject(config.bucket, objectId);

    return {
      id: objectId,
      name: file.metaData['actual-name'],
      size: file.size,
      src: `${config.publicUrl}/${config.bucket}/${objectId}`,
      createdAt: file.lastModified.getDate(),
    }
  } catch {
    return;
  }
}

export async function uploadFile(file: UploadedFile): Promise<IFile> {
  const objectId = (new Date()).getTime().toString(36).slice(3);

  await client.putObject(config.bucket, objectId, file.data, file.size, {
    'Content-Type': file.mimetype,
    'Actual-Name': file.name
  });

  return {
    id: objectId,
    name: file.name,
    size: file.size,
    src: `${config.publicUrl}/${config.bucket}/${objectId}`,
    createdAt: Date.now(),
  };
}

export async function emptyBucket() {
  const files = await listFiles();

  for (const fileIdx in files) {
    const file = files[fileIdx];
    console.log(`Deleting ${Number(fileIdx) + 1}/${files.length} - ${file.id}`);

    await deleteFile(file.id);
  }
}

export async function deleteFile(objectId: string) {
  await client.removeObject(config.bucket, objectId);
}
