module.exports = {
  mode: 'jit',
  purge: [
    './**/*.vue'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      margin: {
        'auto': 'auto'
      },
      colors: {
        neutral: {
          200: '#393940',
          300: '#2a2a30',
          400: '#141417',
          900: '#0e0e10'
        }
      }
    },
    fontFamily: {
      sans: ['Inter var']
    }
  },
  variants: {
    extend: {}
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
  ]
};
