export function usePassword() {
  return process.browser && window.localStorage.getItem('APP_PASSWORD') || '';
}
